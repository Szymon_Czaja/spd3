#include "stdafx.h"
#include "Scheduler.h"


Scheduler::Scheduler()
{
}

void Scheduler::SortDeadline()
{
	std::sort(Data.tasks.begin(), Data.tasks.end(), [](auto& a, auto& b) {
		return a.Deadline < b.Deadline;
	});
}

int Scheduler::WiTi()
{
	do {
		Permutation.push_back(Data.tasks);
	} while (std::next_permutation(Data.tasks.front(), Data.tasks.back()));
	std::vector<int> witi;
	for (auto tasks : Permutation) {
		int pSum = 0;
		for (auto task : tasks.tasks)
			pSum += task.Proces;
		for (auto task : tasks.tasks)
			witi.push_back((pSum - task.Deadline > 0 ? pSum - task.Deadline : 0)*task.Wyte + tasks.GetWiTi());
	}
	int WiTi = *std::min_element(witi.begin(), witi.end());
	return WiTi;
}

int Scheduler::WiTi_Recurention(Tasks perm)
{
	if (perm.tasks.size() != 1) {
		Tasks nowy;
		Tasks nowy2;

		for (int i = 0; i < perm.tasks.size() / 2; ++i)
			nowy.tasks.push_back(perm.tasks[i]);
		for (int i = perm.tasks.size() / 2; i < perm.tasks.size(); ++i)
			nowy2.tasks.push_back(perm.tasks[i]);
		WiTi_Recurention(nowy);
		WiTi_Recurention(nowy2);
	}
	return perm.GetWiTi();
}


Scheduler::~Scheduler()
{
}


