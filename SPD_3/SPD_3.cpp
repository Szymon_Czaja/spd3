// SPD_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


auto loadfile(const std::string& name)
{
	Tasks tasklist;
	std::fstream plik{ name, std::ios::in };
	int size, s, e, d;;
	plik >> size;
	for (int i = 0; i < size; ++i) {
		plik >> s >> e >> d;
		tasklist.tasks.emplace_back(s, e, d);
	}
	return tasklist;
}


int main()
{
	int a;
	Tasks lista = loadfile("data10.txt");
	Scheduler sch{ lista };
	std::cout << lista.witi() << std::endl;
	std::cout << sch.WiTi() << std::endl;
	std::cin >> a;
    return 0;
}

